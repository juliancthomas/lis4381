<?php
//show errors: at least 1 and 4...
ini_set('display_errors', 1);
//ini_set('log_errors', 1);
//ini_set('error_log', dirname(__FILE__) . '/error_log.txt');
error_reporting(E_ALL);

//use for inital test of form inputs
//exit(print_r($_POST));

$pst_name_v = $_POST['name'];
$pst_street_v = $_POST['street'];
$pst_city_v =$_POST['city'];
$pst_state_v =$_POST['state'];
$pst_zip_v=$_POST['zip'];
$pst_phone_v=$_POST['phone'];
$pst_email_v= $_POST['email'];
$pst_url_v= $_POST['url'];
$pst_ytd_sales_v=$_POST['ytdsales'];
$pst_notes_v=$_POST['notes'];

$pattern='/^[a-zA-Z0-9\-_\s]+$/';
$valid_name=preg_match($pattern, $pst_name_v);

$pattern='/^[a-zA-Z0-9,\s\.]+$/';
$valid_street=preg_match($pattern, $pst_street_v);

$pattern ='/^[a-zA-Z\s]+$/';
$valid_city=preg_match($pattern, $pst_city_v);

$pattern='/^[a-zA-Z]{2,2}+$/';
$valid_state=preg_match($pattern, $pst_state_v);

$pattern='/^\d{5,9}+$/';
$valid_zip=preg_match($pattern, $pst_zip_v);

$pattern='/^\d{10}+$/';
$valid_phone=preg_match($pattern, $pst_phone_v);

$pattern = '/^\d{1,8}(?:\.\d{0,2})?$/';
$valid_ytd_sales=preg_match($pattern, $pst_ytd_sales_v);

echo 'hello <br>'.$valid_ytd_sales.'<br>'.$valid_phone.'<br>'.$valid_zip.'<br>'.$valid_state.'<br>'.$valid_city.'<br>'.$valid_street.'<br>'.$valid_name.'<br>';

echo 'post values <br>'.$pst_name_v.'<br>'.$pst_street_v.'<br>'.$pst_city_v.'<br>'.$pst_state_v.'<br>'.$pst_zip_v.'<br>'.$pst_phone_v.'<br>'.$pst_url_v.'<br>'.$pst_ytd_sales_v.'<br>'.$pst_notes_v.'<br>';
if(
    empty($pst_name_v) ||
    empty($pst_street_v) ||
    empty($pst_city_v) ||
    empty($pst_state_v) ||
    empty($pst_zip_v) ||
    empty($pst_phone_v) ||
    empty($pst_email_v) ||
    empty($pst_url_v)
)
{
    $error = "All field require data, except <b> Notes </b>. Check all fields and try again.";
    include('global/error.php');
}

else if(!is_numeric($pst_ytd_sales_v) || $pst_ytd_sales_v <= 0){
    $error ="YTD Sales can only contain numbers (other than a decimal point); and must be equal to or greater than zero.";
    include('global/error.php');
}

else if($valid_name === false){
    echo 'Error in pattern!';
}

else if ($valid_name === 0){
    $error = 'Invalid name';
    include('global/error.php');
}
//code to process inserts goes here

else if($valid_street === false){
    echo 'Error in pattern!';
}

else if ($valid_street === 0){
    $error = 'Street can only contain numbers, letters, commas, and periods.';
    include('global/error.php');
}

else if($valid_city === false){
    echo 'Error in pattern!';
}

else if ($valid_city === 0){
    $error = 'City can only contain letters.';
    include('global/error.php');
}


else if($valid_state === false){
    echo 'Error in pattern!';
}

else if ($valid_state === 0){
    $error = 'State must contain two letters';
    include('global/error.php');
}

else if($valid_zip === false){
    echo 'Error in pattern!';
}

else if ($valid_zip === 0){
    $error = 'Zip must contain 5 numbers';
    include('global/error.php');
}


else if($valid_phone === false){
    echo 'Error in pattern!';
}

else if ($valid_phone === 0){
    $error = 'Must contain 10 dogits';
    include('global/error.php');
}


else if($valid_ytd_sales === false){
    echo 'Error in pattern!';
}

else if ($valid_ytd_sales === 0){
    $error = 'YTD_Sales must contain no more than 10 digits, including a decimal point.';
    include('global/error.php');
}

else{
    require_once('global/connection.php');


$query="
INSERT INTO petstore
(pst_name, pst_street, pst_city, pst_state, pst_zip, pst_phone, pst_email, pst_url, pst_ytd_sales, pst_notes)
VALUES
(:pst_name_p, :pst_street_p, :pst_city_p, :pst_state_p, :pst_zip_p, :pst_phone_p, :pst_email_p, :pst_url_p, :pst_ytd_sales_p, :pst_notes_p)";

try{
    $statement=$db->prepare($query);
    $statement->bindParam(':pst_name_p', $pst_name_v);
    $statement->bindParam(':pst_street_p', $pst_street_v);
    $statement->bindParam(':pst_city_p', $pst_city_v);
    $statement->bindParam(':pst_state_p', $pst_state_v);
    $statement->bindParam(':pst_zip_p', $pst_zip_v);
    $statement->bindParam(':pst_phone_p', $pst_phone_v);
    $statement->bindParam(':pst_email_p', $pst_email_v);
    $statement->bindParam(':pst_url_p', $pst_url_v);
    $statement->bindParam(':pst_ytd_sales_p', $pst_ytd_sales_v);
    $statement->bindParam(':pst_notes_p', $pst_notes_v);
    $statement ->execute();
    $statement->closeCursor();

    $last_auto_increment_id=$db->lastInsertId();

    header('Location: index.php');
}

catch (PDOException $e){
    $error = $e->getMessage();
    echo $error;
}
}

?>

<!-- CREATE TABLE `petcrud`.`petstore` ( `pst_id` INT NOT NULL AUTO_INCREMENT , `pst_name` VARCHAR(255) NOT NULL , `pst_street` VARCHAR(255) NOT NULL , `pst_city` VARCHAR(255) NOT NULL , `pst_state` CHAR(2) NOT NULL , `pst_zip` TINYINT(9) NOT NULL , `pst_phone` TINYINT(10) NOT NULL , `pst_email` VARCHAR(255) NOT NULL , `pst_url` VARCHAR(255) NOT NULL , `pst_ytd_sales` INT(10) NOT NULL , `pst_notes` VARCHAR(255) NOT NULL , PRIMARY KEY (`pst_id`)) ENGINE = InnoDB;
-->