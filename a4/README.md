# LIS 4381 Mobile Web Application Development

## Julian Thomas

### Assignment #4 Requirements:

*Three Parts*

1. Course title, your name, assignment requirements, as per A1;
2. Screenshots as per below examples;
3. Link to local lis4381 web app: [https://localhost/repos/lis4381](https://localhost/repos/lis4381 "https://localhost/repos/lis4381")

#### Deliverables:
1. Provide Bitbucket read-only access to lis4381 repo, include links to the other assignment repos you created in README.md, using Markdown syntax
2. Blackboard Links: lis4381 Bitbucket repo
3. *Note*: the carousel *must* contain (min. 3) slides that either contain text or images that link to other content areas marketing/promoting your skills. 


#### Assignment Screenshots:

*Screenshot of A4*:

![Screenshot of A4_1](img/a4_1.png)

*Another screenshot of A4*:

![Screenshot of A4_2](img/a4_2.png)

*Another screenshot of A4*:

![Screenshot of A4_3](img/a4_3.png)