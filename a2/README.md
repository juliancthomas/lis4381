> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4381 Mobile Web Application Development

## Julian Thomas

### Assignment #2 Requirements:

#### Requirements:
* 1. Course title, name, assignment requirements, as per A1
* 2. Screenshot of running application’s first user interface
* 3. Screenshot of running application’s second user interface

#### Deliverables:
* 1. Provide Bitbucket read-only access to lis4381 repo, include links to the repos you created in the above tutorials in README.md, using Markdown syntax (README.md must also include screenshots as per above.)
* 2. Blackboard Links: lis4381 Bitbucket repo

> 

#### Assignment Screenshots:

*Screenshot of application’s first user interface*:

![healthy1](img/healthy1.png)

*Screenshot of application’s second user interface*:

![healthy2](img/healthy2.png)


