# LIS 4381 Mobile Web Application Development

## Julian Thomas

### P1 Requirements:


#Part 1
1. Create a launcher icon image and display it in both activities (screens)
2. Must add background color(s) to both activities
3. Must add border around image and button
4. Must add text shadow (button)
Note: Also, review Ch04 PPT file in Notes.


#Part 2
1. Course title, your name, assignment requirements, as per A1;
2. Screenshot of running application’s first user interface;
3. Screenshot of running application’s second user interface;

#### Deliverables:
1. Provide Bitbucket read-only access to lis4381 repo, include links to the repos you created in the above tutorials in README.md must also include links and screenshots as per above.)
2. Blackboard Links: lis4381 Bitbucket repo

*Screenshot of First User Interface*:

![First](1.png) 


*Screenshot of Second user Interface*:

![Second](2.png)

