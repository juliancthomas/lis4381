-- phpMyAdmin SQL Dump
-- version 4.4.15.5
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Nov 22, 2016 at 05:24 AM
-- Server version: 5.6.30
-- PHP Version: 5.5.35

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `petcrud`
--

-- --------------------------------------------------------

--
-- Table structure for table `petstore`
--

CREATE TABLE IF NOT EXISTS `petstore` (
  `pst_id` int(11) NOT NULL,
  `pst_name` varchar(255) NOT NULL,
  `pst_street` varchar(255) NOT NULL,
  `pst_city` varchar(255) NOT NULL,
  `pst_state` char(2) NOT NULL,
  `pst_zip` tinyint(9) NOT NULL,
  `pst_phone` tinyint(10) NOT NULL,
  `pst_email` varchar(255) NOT NULL,
  `pst_url` varchar(255) NOT NULL,
  `pst_ytd_sales` int(10) NOT NULL,
  `pst_notes` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `petstore`
--

INSERT INTO `petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES
(1, 'Julie Riley', '123 Main Street', 'Jacksonville', 'Fl', 127, 127, 'e@g.com', 'y.com', 66, 'Testing');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `petstore`
--
ALTER TABLE `petstore`
  ADD PRIMARY KEY (`pst_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `petstore`
--
ALTER TABLE `petstore`
  MODIFY `pst_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
