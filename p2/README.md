# LIS 4381 Mobile Web Application Development

## Julian Thomas

### Project 2 Requirements:

*Three Parts*
=
1. Course title, your name, assignment requirements, as per A1;
2. Screenshots as per below examples;
3. Link to local lis4381 web app: [https://localhost/repos/lis4381](https://localhost/repos/lis4381 "https://localhost/repos/lis4381")

#### Deliverables:
Deliverables:
1. Provide Bitbucket read-only access to lis4381 repo (Language PHP), include links
to the other assignment repos you created in README.md, using Markdown syntax
(README.md must also include screenshots as per above.)
2. Blackboard Links: lis4381 Bitbucket repo



#### Project 2 Screenshots
*Screenshot of Index*:

![Screenshot of A4_1](img/index.png)


*Screenshot of P2 Edit Petstore*:

![Screenshot of A4_2](img/edit_petstore.png)


*Screenshot of P2 Error*:

![Screenshot of A4_2](img/edit_petstore_process.png)


*Screenshot of RSSFeed*:

![Screenshot of A4_2](img/carousel.png)


*Screenshot of RSSFeed*:

![Screenshot of A4_2](img/rssfeed.png)