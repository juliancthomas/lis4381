> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.

# LIS 4381 Mobile Web Application Development
## Julian Thomas

###Course Work links

#### [A1 README.md](https://bitbucket.org/juliancthomas/lis4381/src/9663e1452b0a2592f945827a850c59b82c7ecb0e/a1/?at=master "README.md")
1. Install AMPPS
2. Install JDK and run java Hello
3. Install Andriod studio and run my first app
4. Provide screenshots of installations
5. Create BitBucket repository links
6. Git commands with short descriptions

#### [A2 README.md](https://bitbucket.org/juliancthomas/lis4381/src/9663e1452b0a2592f945827a850c59b82c7ecb0e/a2/?at=master)
1. Screenshot of running application's first user interface
2. Screenshot of running application's second user interface

#### [A3 README.md](https://bitbucket.org/juliancthomas/lis4381/src/6480a19083278cd81beeec296b354b026115fd40/a3/README.md?at=master&fileviewer=file-view-default)
1. Coursetitle, yourname, assignmentrequirements, as per A1;
2. Screenshot of ERD;
3. Screenshot of running applications first user interface;
4. Screenshot of running applications second user interface;
5. Links to the following files:
	a. [a3.mwb](https://bitbucket.org/juliancthomas/lis4381/src/9663e1452b0a2592f945827a850c59b82c7ecb0e/a3/mwb/a3.mwb?at=master) 
	b. [a3.sql](https://bitbucket.org/juliancthomas/lis4381/src/9663e1452b0a2592f945827a850c59b82c7ecb0e/a3/mwb/a3.sql?at=master&fileviewer=file-view-default)

#### [P1 README.md](https://bitbucket.org/juliancthomas/lis4381/src/566d29ccc683/p1/?at=master)
1. Course title, your name, assignment requirements, as per A1
2. [Screenshot of running application’s first user interface](https://bitbucket.org/juliancthomas/lis4381/src/566d29ccc6837fb8045fa2f44639cfa817763697/p1/1.png?at=master&fileviewer=file-view-default) 
3. [Screenshot of running application’s second user interface](https://bitbucket.org/juliancthomas/lis4381/src/566d29ccc6837fb8045fa2f44639cfa817763697/p1/2.png?at=master&fileviewer=file-view-default)


#### [A4 README.md](https://bitbucket.org/juliancthomas/lis4381/src/73215ad81acacfdc5ae691b974c5ff96c3f658d9/a4/?at=master)
1. TBDCd c:/webdev/repos/crsnnnn
2. Close assignment starter files
3. Review subdirectories and files
4. Open index.php and review code



#### [A5 README.md](https://bitbucket.org/juliancthomas/lis4381/src/81b2ec089fb24ebc6ba8cc0f16c67c770960fb46/a5/?at=master)
1. Requires A4 cloned files
2. Open index.php and review code
3. Server Side Validation

#### [P2 README.md](https://bitbucket.org/juliancthomas/lis4381/src/ee05ccda644a8906ed5d12445a078779b9e0e1e8/p2/?at=master)
1. Requires A4 cloned files.
2. Review subdirectories and files
3. Code CRUD System

