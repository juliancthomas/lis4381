# LIS 4381 Mobile Web Application Development

## Julian Thomas

### Assignment #3 Requirements:

*Three Parts*

1. Coursetitle, yourname, assignment requirements, as per A1;
2. Screenshot of ERD;
3. Screenshot of running applicationâ€™s first user interface;
4. Screenshot of running applicationâ€™s second user interface;
5. Links to the following files:
	a. a3.mwb
	b. a3.sql

#### Deliverables:
1. Provide Bitbucket read-only access to lis4381 repo, include links to the repos you created in the above tutorials in README.md must also include links and screenshots as per above.)
2. Blackboard Links: lis4381 Bitbucket repo

*Link to A3.mwb*:

[Link to a3.mwb](mwb/a3.mwb)



*Link to A3.sql*:

[Link to a3.sql](mwb/a3.sql)


*Screenshot of A3.mwb*:

![Screenshot of A3.mwb](img/a3.png)

*Another screenshot of A3.mwb working*:

![Screenshot of A3.mwb](img/a3_2.png)

*Screenshot of application's first user interface*:

![Screenshot of application's first user interface](img/screen1.png)

*Screenshot of application's second user interface*:

![Screenshot of application's second user interface](img/screen2.png)
0
